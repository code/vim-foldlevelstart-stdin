"
" foldlevelstart_stdin.vim: Set 'foldlevel' to 'foldlevelstart' after reading
" from standard input, which Vim doesn't do by default.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_foldlevelstart_stdin') || &compatible || v:version < 700
  finish
endif
let loaded_foldlevelstart_stdin = 1

" Watch for stdin reads and set fold level accordingly
augroup foldlevelstart_stdin
  autocmd!
  autocmd StdinReadPre *
        \ let &l:foldlevel = max([&foldlevelstart, &g:foldlevel, 0])
augroup END
