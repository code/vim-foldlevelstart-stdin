foldlevelstart\_stdin.vim
=========================

This tiny plugin sets `'foldlevel'` to `'foldlevelstart'` after reading from
standard input, which Vim doesn't do by default.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
